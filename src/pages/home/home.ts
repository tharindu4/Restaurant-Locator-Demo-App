import { Component, ViewChild ,ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Geolocation ,GeolocationOptions ,Geoposition ,PositionError } from '@ionic-native/geolocation'; 
import { AlertController } from 'ionic-angular';

declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  options : GeolocationOptions;
  currentPos : Geoposition;
  places : Array<any>; 
  lat : number;
  lng : number;
  currentInfoWindow:any; 
  tempPlace : any;

  constructor(public navCtrl: NavController,private geolocation : Geolocation, private alertCtrl: AlertController) {
    console.log("Home Constructor");
    this.currentInfoWindow = null;
    // this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(
    //   result => console.log('Has permission?',result.hasPermission),
    //   err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION)
    // );
  } 

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  getUserPosition(){
    this.options = {
    enableHighAccuracy : false
    };
    this.geolocation.getCurrentPosition(this.options).then((pos : Geoposition) => {
        this.currentPos = pos;     
        this.lat = pos.coords.latitude;
        this.lng = pos.coords.longitude;
        console.log(pos);
        this.addMap(pos.coords.latitude,pos.coords.longitude);

    },(err : PositionError)=>{
        console.log("error : " + err.message);
    ;
    })
  }

  addMap(lat,long){

    let latLng = new google.maps.LatLng(lat, long);

    let mapOptions = {
    center: latLng,
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    this.addMarker();
  }

  addMarker(){

    let marker = new google.maps.Marker({
    map: this.map,
    animation: google.maps.Animation.DROP,
    position: this.map.getCenter()
    });

    let content = "<p>This is your current position !</p>";          
    let infoWindow = new google.maps.InfoWindow({
    content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      if (this.currentInfoWindow != null) { 
        this.currentInfoWindow.close(); 
      } 
      infoWindow.open(this.map, marker); 
      this.currentInfoWindow = infoWindow; 
    });

  }

  getNearbyRestaurants(latLng){
      var service = new google.maps.places.PlacesService(this.map);
      let request = {
          location : latLng,
          radius : 2000 ,
          types: ["restaurant"]
      };
      return new Promise((resolve,reject)=>{
          service.nearbySearch(request,function(results,status){
              if(status === google.maps.places.PlacesServiceStatus.OK)
              {
                  resolve(results);    
              }else
              {
                  reject(status);
              }

          }); 
      });

  }

  createRestaurantMarker(place){
    let marker = new google.maps.Marker({
    map: this.map,
    animation: google.maps.Animation.DROP,
    position: place.geometry.location,
    title: JSON.stringify(place)
    });   

    // let content = "<p>"+place.name+"</p>";          
    // let infoWindow = new google.maps.InfoWindow({
    // content: content
    // });

    google.maps.event.addListener(marker, 'click', () => {
      // code for showing infoWindow
      // if (this.currentInfoWindow != null) { 
      //   this.currentInfoWindow.close(); 
      // } 
      // infoWindow.open(this.map, marker); 
      // this.currentInfoWindow = infoWindow; 
      // code for showing infoWindow
      
      this.showAlert(marker.title)
    });
  }   

  showNearbyRestaurants(lat,long){
    console.log("Function Working");
    let latLng = new google.maps.LatLng(lat, long);

    this.getNearbyRestaurants(latLng).then((results : Array<any>)=>{
        this.places = results;
        for(let i = 0 ;i < results.length ; i++)
        {
            this.createRestaurantMarker(results[i]);
        }
    },(status)=>console.log(status));
  }

  showAlert(place) {
    // console.log(place);
    this.tempPlace = JSON.parse(place);
    let alert = this.alertCtrl.create({
      title: this.tempPlace.name,
      subTitle: this.tempPlace.vicinity,
      buttons: ['Close']
    });
    alert.present();
  }

  ionViewDidEnter(){
    this.getUserPosition();
  } 

}
